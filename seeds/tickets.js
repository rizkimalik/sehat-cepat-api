
exports.seed = async function (knex) {
    let fake_data = [];
    for (let i = 1; i <= 1000000; i += 1) {
        fake_data.push({
            customer_id: '230203012448',
            ticket_number: `28002030200${i}`,
            group_ticket_number: '',
            ticket_source: 'Email',
            status: 'Open',
            category_id: 'CAT-10001',
            category_sublv1_id: 'CT1-10007',
            category_sublv2_id: 'CT2-20003',
            category_sublv3_id: 'CT2-20003',
            complaint_detail: `complaint test ${i}`,
            response_detail: `response test ${i}`,
            sla: '5',
            ticket_position: 'L1',
            dispatch_department: '6',
            department_id: '2',
            ticket_type: '',
            mass_distruption: '0',
            source_information: 'Mailbox',
            cust_name: 'test',
            cust_email: 'test@mail.com',
            cust_telephone: 'test',
            cust_address: 'test',
            thread_id: `12330200${i}`,
            user_create: 'Agent1',
            date_create: knex.fn.now(),
            user_closed: 'Agent1',
            date_closed: knex.fn.now(),
            last_response_by: 'Agent1',
            last_response_at: knex.fn.now(),
        });

        if (i % 1000 === 0) {
            console.log('row: '+i)
            await knex('tickets').insert(fake_data);
            fake_data = [];
        }
    }
}
