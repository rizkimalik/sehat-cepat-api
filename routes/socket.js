'use strict';

const { update_socket } = require("../controller/omnichannel/chat");
const socket_chat = require("../controller/socket/socket_chat");
const socket_whatsapp = require("../controller/socket/socket_whatsapp");
const socket_twitter = require("../controller/socket/socket_twitter");
const socket_email = require("../controller/socket/socket_email");
const socket_facebook = require("../controller/socket/socket_facebook");
const socket_instagram = require("../controller/socket/socket_instagram");
const socket_general = require("../controller/socket/socket_general");
const socket_telegram = require("../controller/socket/socket_telegram");

//? socket.broadcast.emit = public chat
//? socket.to(room).emit = private chat
module.exports = function (io) {
    //? middleware auth.username
    io.use((socket, next) => {
        const { flag_to, username, email } = socket.handshake.auth;
        if (!username) {
            const err = new Error("not authorized");
            next(err);
        }
        else {
            socket.username = username;
            socket.flag_to = flag_to;
            socket.email = email;
            next();
            console.log(`✅[${flag_to}] ${username} - ${email} : ID ${socket.id}, auth success`);
        }
    });

    io.on('connection', (socket) => {
        let users = {}
        users[socket.id] = socket.username;

        if (socket.flag_to === 'customer') {
            socket.join(socket.email);
        } else {
            socket.join(socket.username);
            update_socket({ username: socket.username, flag_to: socket.flag_to, email: socket.email, uuid: socket.id, connected: socket.connected, login: 1, aux: 1 });
        }

        socket_chat(socket); //module chat
        socket_whatsapp(socket); //module whatsapp
        socket_twitter(socket); //module twitter
        socket_email(socket); //module email
        socket_facebook(socket); //module facebook
        socket_instagram(socket); //module instagram
        socket_telegram(socket); //module telegram
        socket_general(socket); //module general

        socket.on('disconnect', (res) => {
            const { username, flag_to, email } = socket.handshake.auth;
            if (username || email) {
                socket.to(socket.username).emit('return-disconnect', socket.username);
                update_socket({ username, flag_to, email, uuid: socket.id, connected: socket.connected, login: 0, aux: 2 });
            }
            delete users[socket.id];
            console.log(`⛔[${flag_to}] ${username} - ${email} : ID ${socket.id}, connected:${socket.connected}, ${res} `);
        });
    });



}