'use strict';
const knex = require('../config/db_connect');
const { auth_jwt_bearer } = require('../middleware');
const logger = require('../helper/logger');
const response = require('../helper/json_response');

const index = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY id DESC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `WHERE ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
            SELECT a.*,b.name FROM customer_channels a
            INNER JOIN customers b ON b.customer_id=a.customer_id
            ${filtering} ${orderby} LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const total = await knex.raw(`
            SELECT count(*) as total FROM customer_channels a 
            INNER JOIN customers b ON b.customer_id=a.customer_id 
            ${filtering}
        `);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        logger('customer_channel/index', error);
        res.status(500).end();
    }
}

const data_channel_customer = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, customer_id } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 5;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY id DESC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
            SELECT * FROM customer_channels WHERE customer_id='${customer_id}' 
            ${filtering} ${orderby} LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const total = await knex.raw(`SELECT COUNT(*) AS total from customer_channels WHERE customer_id='${customer_id}' ${filtering}`);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error, 'customer_channel/data_channel_customer');
    }
}

const add_channel_customer = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const { customer_id, value_channel, flag_channel } = req.body;
        const check = await knex('customer_channels').where({ value_channel, flag_channel });
        if (check.length === 0) {
            if (value_channel) {
                await knex('customer_channels')
                    .insert([{
                        customer_id,
                        flag_channel,
                        value_channel,
                        created_at: knex.fn.now()
                    }]);
                response.ok(res, 'success insert data.');
            }
            else{
                response.ok(res, 'value data empty.');
            }
        }
        else {
            response.ok(res, 'data allready exist.');
        }
    }
    catch (error) {
        logger('customer_channel/store', error);
    }
}

const update_channel_customer = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            id,
            // flag_channel,
            value_channel
        } = req.body;

        const result = await knex('customer_channels')
            .update({
                // flag_channel,
                value_channel,
                updated_at: knex.fn.now()
            })
            .where({ id });
        response.ok(res, result);
    }
    catch (error) {
        logger('customer_channel/delete_channel_customer', error);
        res.status(500).end();
    }
}

const delete_channel_customer = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const { id } = req.body; //? del by id tbl
        const result = await knex('customer_channels').where({ id }).del();
        response.ok(res, result);
    }
    catch (error) {
        logger('customer_channel/delete_channel_customer', error);
        res.status(500).end();
    }
}


//? Non HTTP
const insert_channel_customer = async function (req) {
    try {
        const { customer_id, value_channel, flag_channel } = req;
        const check = await knex('customer_channels').where({ value_channel, flag_channel });
        if (check.length === 0) {
            if (value_channel) {
                await knex('customer_channels')
                    .insert([{
                        customer_id,
                        flag_channel,
                        value_channel,
                        created_at: knex.fn.now()
                    }]);
            }
        }
    }
    catch (error) {
        logger('customer_channel/store', error);
    }
}

const destroy_channel_customer = async function (req, res) {
    try {
        const { customer_id } = req;
        const result = await knex('customer_channels').where({ customer_id }).del();
        return result;
    }
    catch (error) {
        logger('customer_channel/destroy_channel_customer', error);
    }
}

module.exports = {
    index,
    data_channel_customer,
    add_channel_customer,
    update_channel_customer,
    delete_channel_customer,
    insert_channel_customer,
    destroy_channel_customer,
}