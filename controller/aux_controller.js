const knex = require('../config/db_connect');
const { response, logger } = require('../helper');

const index = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const { data } = req.query;

        let result;
        if (data == 'active') {
            result = await knex('aux_status').where({ active: '1' }).orderBy('id', 'asc');
        }
        else {
            result = await knex('aux_status').orderBy('id', 'asc');
        }
        response.ok(res, result);
    }
    catch (error) {
        logger('aux/index', error);
        res.status(500).end();
    }
}

const store = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            name,
            description,
            created_at = knex.fn.now(),
        } = req.body;
        const aux = await knex('aux').select('aux_id').orderBy('aux_id', 'desc').first();

        let auxid;
        if (Boolean(aux === false)) {
            auxid = `CAT-10001`;
        }
        else {
            const aux_no = Number(aux.aux_id.split('-')[1]) + 1;
            auxid = `CAT-${aux_no}`;
        }

        await knex('aux')
            .insert([{
                aux_id: auxid,
                name,
                description,
                created_at
            }]);
        response.ok(res, auxid);
    }
    catch (error) {
        logger('aux/store', error);
        res.status(500).end();
    }
}

const update = async function (req, res) {
    try {
        if (req.method !== 'PUT') return res.status(405).end('Method not Allowed');
        const {
            aux_id,
            name,
            description,
            updated_at = knex.fn.now()
        } = req.body;

        await knex('aux')
            .update({
                name,
                description,
                updated_at
            })
            .where({ aux_id });
        response.ok(res, 'success update');
    }
    catch (error) {
        logger('aux/update', error);
        res.status(500).end();
    }
}

const update_aux_user = async function (req, res) {
    try {
        if (req.method !== 'PUT') return res.status(405).end('Method not Allowed');
        const { username, aux } = req.body;

        await knex('users').update({ aux }).where({ username });

        const { aux_name } = await knex('aux_status').where({ id: aux }).first();
        await knex('aux_history')
            .insert([{
                username: username,
                aux_id: aux,
                aux_name: aux_name,
                aux_date: knex.fn.now(),
            }]);
        response.ok(res, aux_name);
    }
    catch (error) {
        logger('user/update_aux_user', error);
        res.status(500).end();
    }
}

const status_aux_user = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end('Method not Allowed');
        const { username } = req.query;

        const user = await knex('users').select('username','aux').where({ username }).first();
        const { aux_name } = await knex('aux_status').where({ id: user.aux }).first();
        user.aux_status = aux_name;
        response.ok(res, user);
    }
    catch (error) {
        logger('user/status_aux_user', error);
        res.status(500).end();
    }
}

module.exports = {
    index,
    store,
    update,
    update_aux_user,
    status_aux_user,
}