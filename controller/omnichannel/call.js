'use strict';
const fs = require('fs');
const axios = require('axios');
const https = require('https');
const date = require('date-and-time');
const knex = require('../../config/db_connect');
const { logger, response } = require('../../helper');
const { auth_jwt_bearer } = require('../../middleware');
const { insert_channel_customer } = require('../customer_channel_controller');

const httpsAgent = new https.Agent({
    rejectUnauthorized: false, // (NOTE: this will disable client verification)
    cert: fs.readFileSync('ssl/certificate_pabx.crt', 'utf8'),
    key: fs.readFileSync('ssl/private_pabx.key', 'utf8'),
});

//? HTTP FUNCTION
const insert_call_history = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const {
            // customer_id,
            interaction_id,
            phone_number,
            agent_handle,
        } = req.body;

        // check data customer
        let customer_id = await insert_data_customer(req.body);
        const call = await knex('call_history').select('interaction_id').where({ interaction_id }).first();
        if (!call && interaction_id) {
            await knex('call_history')
                .insert([{
                    interaction_id,
                    customer_id,
                    phone_number,
                    agent_handle,
                    call_type: 'IN',
                    datetime_start: knex.fn.now()
                }]);

            response.ok(res, 'insert success.');
        }
        else {
            response.ok(res, 'data allredy exists.');
        }
    }
    catch (error) {
        logger('ERROR/call/insert_call_history', error.message);
    }
}

const insert_call_detailrecords = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            session_id,
            call_id,
            callee,
            callee_domain,
            caller,
            caller_display_name,
            caller_domain,
            ring_time,
            answered_time,
            start_time,
            ended_time,
            ended_reason,
            event_type,
            direction,
            status_code,
            tenant_id,
            tenant_name
        } = req.body;

        if (session_id) {
            await knex('call_detail_records')
                .insert([{
                    session_id,
                    call_id,
                    callee,
                    callee_domain,
                    caller,
                    caller_display_name,
                    caller_domain,
                    ring_time,
                    answered_time,
                    start_time,
                    ended_time,
                    ended_reason,
                    event_type,
                    direction,
                    status_code,
                    tenant_id,
                    tenant_name
                }]);
            response.ok(res, 'insert success.');
        }
        else {
            response.ok(res, 'data allredy exists.');
        }
    }
    catch (error) {
        logger('ERROR/call/insert_call_detailrecords', error.message);
    }
}

const insert_data_customer = async function (data) {
    const now = new Date();
    const generate_customerid = date.format(now, 'YYMMDDHHmmSS');
    const customer = await knex('customers').select('customer_id').where({ phone_number: data.phone_number }).first();
    const channel = await knex('customer_channels').select('customer_id').where({ value_channel: data.phone_number }).first();
    const customer_id = customer ? customer.customer_id : generate_customerid;

    if (!customer) {
        await knex('customers')
            .insert([{
                customer_id: customer_id,
                name: 'pbx_call_in',
                phone_number: data.phone_number,
                source: 'PBX',
                status: 'Initialize',
                created_at: knex.fn.now()
            }]);
    }
    if (!channel) {
        await insert_channel_customer({ customer_id, value_channel: data.phone_number, flag_channel: 'Phone' });
    }
    return customer_id;
}

const data_call_history = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY id DESC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `WHERE ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
            SELECT * FROM v_call_detail_records 
            ${filtering} ${orderby} LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const total = await knex.raw(`SELECT COUNT(*) AS total from v_call_detail_records ${filtering}`);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        logger('ERROR/call/data_call_history', error.message);
    }
}

const data_total_call = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        const result = await knex.raw(`SELECT COUNT(interaction_id) as total_call FROM v_call_detail_records`);
        response.ok(res, result[0][0].total_call);
    }
    catch (error) {
        logger('ERROR/call/data_total_call', error.message);
        res.status(500).end();
    }
}

module.exports = {
    insert_call_history,
    insert_call_detailrecords,
    data_call_history,
    data_total_call,
}
