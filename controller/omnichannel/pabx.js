'use strict';
const fs = require('fs');
const axios = require('axios');
const https = require('https');
const knex = require('../../config/db_connect');
const { logger, response, expensive_fn } = require('../../helper');

const httpsAgent = new https.Agent({
    rejectUnauthorized: false, // (NOTE: this will disable client verification)
    cert: fs.readFileSync('ssl/certificate_pabx.crt', 'utf8'),
    key: fs.readFileSync('ssl/private_pabx.key', 'utf8'),
});

const pabx_get_token = async function (req, res) {
    try {
        const account = await knex('pbx_accounts').where({ active: '1' }).first();
        if (account) {
            await axios({
                method: 'post',
                url: `${account.api_url}/tokens/by_extension`,
                httpsAgent,
                headers: {
                    'Content-Type': 'application/json'
                },
                data: JSON.stringify({
                    extension_number: `${account.extension_number}`,
                    password: `${account.password}`,
                    domain: `${account.domain}`
                })
            })
                .then(async function (result) {
                    result.data.api_url = account.api_url;
                    response.ok(res, result.data);
                    return result.data;
                })
                .catch(function (error) {
                    response.error(res, error.message, 'ERROR/omnichannel/pabx_get_token');
                });
        }
        else {
            response.error(res, 'account not found.', 'ERROR/omnichannel/pabx_get_token');
        }
    }
    catch (error) {
        response.error(res, error.message, 'ERROR/omnichannel/pabx_get_token');
    }
}

const pabx_get_recording = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const { token, interaction_id } = req.body;
        const account = await knex('pbx_accounts').where({ active: '1' }).first();

        await axios({
            method: 'get',
            url: `${account.api_url}/recordings/${interaction_id}`,
            httpsAgent,
            headers: {
                'Authorization': `Bearer ${token}`
            }
        })
            .then(async function (result) {
                // return result.data;
                response.ok(res, result.data);
            })
            .catch(function (error) {
                console.log(error);
                logger('ERROR/omnichannel/pabx_get_recording', error);
            });
    }
    catch (error) {
        logger('ERROR/omnichannel/pabx_get_recording', error);
    }
}

const pabx_get_extensions = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end('Method not Allowed');
        const account = await knex('pbx_accounts').where({ active: '1' }).first();
        const extension = await knex('pbx_extensions').where({ extension_type: 'cust' }).orderBy('id', 'asc');
        const token = await axios({
            method: 'POST',
            url: `${account.api_url}/tokens/by_extension`,
            httpsAgent,
            data: JSON.stringify({
                extension_number: `${account.extension_number}`,
                password: `${account.password}`,
                domain: `${account.domain}`
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        }).catch((error) => {
            response.error(res, error.message, 'ERROR/omnichannel/pabx_get_extensions')
        });

        if (!token.data.access_token) return response.error(res, 'Failed get token.', 'ERROR/omnichannel/pabx_get_extensions');
        for (let i = 0; i < extension.length; i++) {
            await axios({
                method: 'get',
                url: `${account.api_url}/users/${extension[i].extension_id}/status`,
                httpsAgent,
                headers: {
                    'Authorization': `Bearer ${token.data.access_token}`
                }
            })
                .then(async (result) => {
                    // console.log(result.data);
                    expensive_fn(i);
                    if (result.data.status === 'OFFLINE') {
                        console.log(extension[i]);
                        extension[i].vdn_number = '101';
                        response.ok(res, extension[i]);
                    }
                })
                .catch((error) => {
                    console.log(error);
                    response.error(res, error.message, 'ERROR/omnichannel/pabx_get_extensions');
                });
        }
    }
    catch (error) {
        response.error(res, error.message, 'ERROR/omnichannel/pabx_get_extensions');
    }
}

module.exports = {
    pabx_get_token,
    pabx_get_recording,
    pabx_get_extensions,
}
