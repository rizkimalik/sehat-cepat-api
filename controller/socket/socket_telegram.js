'use strict';
const { telegram_post_message } = require("../omnichannel/telegram");

module.exports = function (socket) {
    socket.on('send-message-telegram', async (data) => {
        await telegram_post_message(data);
        socket.to(data.chat_id).to(data.agent_handle).emit('return-message-telegram', data);
    });

    socket.on('return-message-telegram', (data) => {
        socket.to(data.chat_id).to(data.agent_handle).emit('return-message-telegram', data); //notused
    });
}