'use strict';
const { instagram_post_messenger, instagram_post_feed } = require("../omnichannel/instagram");

module.exports = function (socket) {
    socket.on('send-instagram-messenger', (data) => {
        instagram_post_messenger(data);
        socket.to(data.chat_id).to(data.agent_handle).emit('return-instagram-messenger', data);
    });
    
    socket.on('return-instagram-messenger', (data) => {
        socket.to(data.chat_id).to(data.agent_handle).emit('return-instagram-messenger', data); //notused
    });

    socket.on('send-instagram-feed', (data) => {
        instagram_post_feed(data);
        socket.to(data.chat_id).to(data.agent_handle).emit('return-instagram-feed', data);
    });
    
    socket.on('return-instagram-feed', (data) => {
        socket.to(data.chat_id).to(data.agent_handle).emit('return-instagram-feed', data); //notused
    });

}