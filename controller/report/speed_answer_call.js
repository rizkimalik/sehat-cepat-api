'use strict';
const knex = require('../../config/db_connect');
const { auth_jwt_bearer } = require('../../middleware');
const { logger, response } = require('../../helper');

const speed_answer_call = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { action } = req.body;

        let date_range = '';
        if (action === 'Today') {
            date_range = 'DATE(from_unixtime(ring_time)) = DATE(NOW())';
        }
        else if (action === 'Last7Days') {
            date_range = 'DATE(from_unixtime(ring_time)) BETWEEN DATE_SUB(DATE(NOW()), INTERVAL 7 DAY) AND DATE(NOW())';
        }

        const result = await knex.raw(`
            SELECT time_range, 
                    total_count, 
                    SUM(total_count) OVER () AS total_sum,
                    ROUND((total_count / SUM(total_count) OVER () * 100), 2) AS percentage
            FROM (
                SELECT '5 second' AS time_range, COUNT(*) AS total_count FROM call_detail_records
                WHERE (answered_time - ring_time) >= 0 AND (answered_time - ring_time) <= 5 AND answered_time <> 0 AND ${date_range}
                UNION ALL
                SELECT '10 second' AS time_range, COUNT(*) AS total_count FROM call_detail_records
                WHERE (answered_time - ring_time) > 5 AND (answered_time - ring_time) <= 10 AND answered_time <> 0 AND ${date_range}
                UNION ALL
                SELECT '20 second' AS time_range, COUNT(*) AS total_count FROM call_detail_records
                WHERE (answered_time - ring_time) > 10 AND (answered_time - ring_time) <= 20 AND answered_time <> 0 AND ${date_range}
                UNION ALL
                SELECT '30 second' AS time_range, COUNT(*) AS total_count FROM call_detail_records
                WHERE (answered_time - ring_time) > 20 AND (answered_time - ring_time) <= 30 AND answered_time <> 0 AND ${date_range}
                UNION ALL
                SELECT '40 second' AS time_range, COUNT(*) AS total_count FROM call_detail_records
                WHERE (answered_time - ring_time) > 30 AND (answered_time - ring_time) <= 40 AND answered_time <> 0 AND ${date_range}
                UNION ALL
                SELECT '50 second' AS time_range, COUNT(*) AS total_count FROM call_detail_records
                WHERE (answered_time - ring_time) > 40 AND (answered_time - ring_time) <= 50 AND answered_time <> 0 AND ${date_range}
                UNION ALL
                SELECT '60 second' AS time_range, COUNT(*) AS total_count FROM call_detail_records
                WHERE (answered_time - ring_time) > 50 AND (answered_time - ring_time) <= 60 AND answered_time <> 0 AND ${date_range}
                UNION ALL
                SELECT '60 second +' AS time_range, COUNT(*) AS total_count FROM call_detail_records
                WHERE (answered_time - ring_time) > 60  AND answered_time <> 0 AND ${date_range}
            ) subquery
            ORDER BY CAST(SUBSTRING(time_range, 1, POSITION(' ' IN time_range)) AS INTEGER)
        `);
        response.ok(res, result[0]);
    }
    catch (error) {
        logger('report/speed_answer_call', error);
        res.status(500).end();
    }
}

module.exports = { speed_answer_call }