
const up = function(knex) {
    return knex.schema.createTable('priority_scale', function(table){
        table.increments('id').primary();
        table.string('priority_scale', 50).unique();
        table.text('description');
        table.boolean('active');
        table.string('created_by', 50);
        table.string('updated_by', 50);
        table.timestamps();
    })
};

const down = function(knex) {
    return knex.schema.dropTable('priority_scale');
};

module.exports = {up, down}