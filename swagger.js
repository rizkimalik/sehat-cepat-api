const swaggerJsdoc = require('swagger-jsdoc');

const options = {
    swaggerDefinition: {
        info: {
            title: 'Keurais API Swagger',
            version: '1.0.5',
            description: 'Documentation for API Keurais Case Management',
        },
        // openapi: '3.0.0',
        basePath: '/',
    },
    apis: [
        './controller/*.js', 
        './controller/master_data/*.js'
    ], // Ubah ini sesuai dengan path ke file-file route Anda
};

const swaggerSpecs = swaggerJsdoc(options);

module.exports = swaggerSpecs;
